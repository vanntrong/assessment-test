export interface Word {
  id: string;
  index: string;
  score: number;
  word: string;
}
