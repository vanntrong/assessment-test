import axios from "axios";
import { BASE_API_URL } from "../constants/env";

export const baseQuery = axios.create({
  baseURL: BASE_API_URL,
});

baseQuery.interceptors.response.use((response) => {
  return response.data;
});
