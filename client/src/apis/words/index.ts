import { baseQuery } from "..";

export const wordsApis = {
  getWords: (text: string) => baseQuery.get("/", { params: { text } }),

  addWord: (text: string) => baseQuery.post("/", { text }),

  deleteWord: (text: string) => baseQuery.delete("/", { params: { text } }),
};
