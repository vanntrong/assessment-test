import React, { useEffect, useState } from "react";
import { wordsApis } from "./apis/words";
import "./App.css";
import WordBar from "./components/add-word-bar";
import SearchBar from "./components/search-bar";
import { Word } from "./interfaces/word.interface";

function App() {
  const [words, setWords] = useState<Word[]>([]);
  const [searchValue, setSearchValue] = useState<string>("");

  const fetchWords = async (search: string) => {
    // note: will update debounce function later
    const res = await wordsApis.getWords(search);
    setWords(res.data);
  };

  const handleAddWord = async (word: string) => {
    const res = await wordsApis.addWord(word);
    // setWords(res.data);
    alert("Added word successfully");
    setSearchValue(res.data.word);
  };

  const handleDeleteWord = async (word: string) => {
    const res = await wordsApis.deleteWord(word);
    alert(`Deleted ${res.data.deleted} words like ${word} successfully`);
  };

  useEffect(() => {
    if (!searchValue) {
      setWords([]);
      return;
    }
    fetchWords(searchValue);
  }, [searchValue]);

  return (
    <div className="App">
      <div className="container">
        <WordBar
          title="Add New Word"
          placeholder="Enter New Word"
          onConfirm={handleAddWord}
        />
        <WordBar
          title="Delete Word"
          placeholder="Enter Delete Word"
          onConfirm={handleDeleteWord}
        />
        <SearchBar
          data={words}
          searchValue={searchValue}
          onSearch={(value) => setSearchValue(value)}
        />
      </div>
    </div>
  );
}

export default App;
