import React, { ChangeEvent, useState } from "react";
import "./index.css";
import { BsSearch } from "react-icons/bs";
import { AiOutlineClose } from "react-icons/ai";
import { Word } from "../../interfaces/word.interface";

interface Props {
  data: Word[];
  searchValue?: string;
  onSearch?: (value: string) => void;
}

function SearchBar({ data, searchValue, onSearch }: Props) {
  const handleFilter = (event: ChangeEvent<HTMLInputElement>) => {
    const searchWord = event.target.value;
    onSearch?.(searchWord);
  };

  const clearInput = () => {
    onSearch?.("");
  };

  return (
    <div className="search">
      <div className="searchInputs">
        <input
          type="text"
          placeholder={"Write something"}
          value={searchValue}
          onChange={handleFilter}
        />
        <div className="searchIcon">
          {searchValue?.length === 0 ? (
            <BsSearch color="black" />
          ) : (
            <AiOutlineClose onClick={clearInput} color="black" />
          )}
        </div>
      </div>
      {data.length !== 0 && (
        <div className="dataResult">
          {data.map((value) => {
            return (
              //   <a className="dataItem" href={value.link} target="_blank">
              //   </a>
              <p className="dataItem" key={value.id}>
                {value.word}
              </p>
            );
          })}
        </div>
      )}
      {data.length === 0 && (searchValue || "").length > 0 && (
        <p>No results found</p>
      )}
    </div>
  );
}

export default SearchBar;
