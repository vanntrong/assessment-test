import React, { useState } from "react";
import "./index.css";

interface Props {
  title: string;
  placeholder: string;
  onConfirm: (value: string) => void;
}

const WordBar = ({ title, placeholder, onConfirm }: Props) => {
  const [value, setValue] = useState("");
  return (
    <div className="wrapper">
      <input
        type="text"
        placeholder={placeholder}
        value={value}
        onChange={(e) => setValue(e.target.value)}
      />
      <button
        disabled={value.length === 0}
        onClick={() => {
          onConfirm(value);
          setValue("");
        }}
      >
        {title}
      </button>
    </div>
  );
};

export default WordBar;
