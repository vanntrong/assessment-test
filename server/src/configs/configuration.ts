export default () => ({
  port: +process.env.PORT || 3000,
  es: {
    cloudId: process.env.ES_CLOUD_ID,
    username: process.env.ES_USERNAME,
    password: process.env.ES_PASSWORD,
  },
});
