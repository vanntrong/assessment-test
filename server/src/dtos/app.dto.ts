import { IsString, IsNotEmpty } from 'class-validator';

export class SearchDto {
  @IsString()
  @IsNotEmpty()
  text: string;
}

export class CreateDto {
  @IsString()
  @IsNotEmpty()
  text: string;
}

export class DeleteDto {
  @IsString()
  @IsNotEmpty()
  text: string;
}
