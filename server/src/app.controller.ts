import { Body, Controller, Delete, Get, Post, Query } from '@nestjs/common';
import { AppService } from './app.service';
import { esIndex } from './constants';
import { CreateDto, DeleteDto, SearchDto } from './dtos/app.dto';
import { ESService } from './elastic-search/elastic-search.service';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly esService: ESService,
  ) {}

  @Get()
  search(@Query() query: SearchDto) {
    return this.esService.search(esIndex, query.text);
  }

  @Post()
  create(@Body() body: CreateDto) {
    return this.esService.create(esIndex, body.text);
  }

  @Delete()
  delete(@Query() query: DeleteDto) {
    return this.esService.delete(esIndex, query.text);
  }
}
