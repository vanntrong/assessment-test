import { Global, Module } from '@nestjs/common';
import { ESService } from './elastic-search.service';

@Global()
@Module({
  providers: [ESService],
  exports: [ESService],
})
export class ESModule {}
