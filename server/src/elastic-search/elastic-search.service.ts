import { Client } from '@elastic/elasticsearch';
import { Injectable, Logger } from '@nestjs/common';
import * as fs from 'fs';
import configuration from 'src/configs/configuration';

@Injectable()
export class ESService {
  logger: Logger;
  esClient: Client;
  constructor() {
    this.logger = new Logger(ESService.name);
    const config = configuration();
    try {
      this.esClient = new Client({
        cloud: {
          id: config.es.cloudId,
        },
        auth: {
          username: config.es.username,
          password: config.es.password,
        },
      });
      this.logger.log('ElasticSearch client connected');
    } catch (error) {
      this.logger.error(error);
      throw error;
    }
  }

  /**
   *
   * @param index - index name
   * @param filePath - path to file
   * @description - Load data to index from file
   */
  loadData(index: string, filePath: string) {
    try {
      // remove duplicates
      const rawData = new Set(fs.readFileSync(filePath, 'utf8').split(' '));
      const body = new Array(...rawData).flatMap((word) => [
        { index: { _index: index } },
        { word },
      ]);

      this.esClient.bulk({ refresh: true, body: body });
      this.logger.log(`Data loaded into index ${index}`);
    } catch (error) {
      this.logger.error(error);
      throw error;
    }
  }

  /**
   *
   * @param index - index name
   * @param text - text to search
   * @returns - array of words
   */
  async search(index: string, text: string) {
    try {
      const body = {
        query: {
          fuzzy: {
            word: {
              value: text,
              fuzziness: 'AUTO',
              max_expansions: 50,
              prefix_length: 0,
              transpositions: true,
              rewrite: 'constant_score',
            },
          },
        },
      };
      const result = await this.esClient.search<{ word: string }>({
        index,
        body,
        from: 0,
        size: 3,
      });
      this.logger.log(`Search result: ${JSON.stringify({ index, text })}`);
      return {
        data: result.hits.hits.map((item) => ({
          id: item._id,
          index: item._index,
          score: item._score,
          word: item._source.word,
        })),
      };
    } catch (error) {
      throw error;
    }
  }

  /**
   *
   * @param index - index name
   * @param text text to create
   * @returns
   */
  async create(index: string, text: string) {
    try {
      const result = await this.esClient.index({
        index,
        body: {
          word: text,
        },
      });
      this.logger.log(`Created: ${JSON.stringify({ index, text })}`);
      return {
        data: {
          id: result._id,
          index: result._index,
          word: text,
        },
      };
    } catch (error) {
      throw error;
    }
  }

  /**
   *
   * @param index - index name
   * @param text - text to delete
   * @returns
   */
  async delete(index: string, text: string) {
    try {
      const result = await this.esClient.deleteByQuery({
        index,
        body: {
          query: {
            match: {
              word: {
                query: text,
                fuzziness: 'AUTO',
              },
            },
          },
        },
      });
      this.logger.log(
        `Deleted: ${JSON.stringify({ index, text, count: result.deleted })}`,
      );
      return { data: { ...result } };
    } catch (error) {
      throw error;
    }
  }

  /**
   *
   * @param index - index name
   * @returns boolean
   */
  checkIndexExists(index: string) {
    return this.esClient.indices.exists({ index });
  }

  /**
   *
   * @param index - index name
   * @returns
   */
  createIndex(index: string) {
    return this.esClient.indices.create({ index });
  }
}
