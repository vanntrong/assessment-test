import { Logger, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import * as path from 'path';
import { AppModule } from './app.module';
import configuration from './configs/configuration';
import { esIndex } from './constants';
import { ESService } from './elastic-search/elastic-search.service';

async function bootstrap() {
  const config = configuration();
  const logger = new Logger('Bootstrap');
  const app = await NestFactory.create(AppModule, {
    logger: ['error', 'log', 'debug'],
  });
  app.enableCors({
    origin: ['http://localhost:3000'],
  });
  app.useGlobalPipes(new ValidationPipe());
  await app.listen(config.port);
  const esService = app.get<ESService>(ESService);
  const indexExist = await esService.checkIndexExists(esIndex);
  if (!indexExist) {
    await esService.createIndex(esIndex);
    logger.log(`Index ${esIndex} created`);
    esService.loadData(esIndex, path.resolve(__dirname, 'data/data.txt'));
  }
  logger.log(`Application listening on port ${config.port}`);
}
bootstrap();
